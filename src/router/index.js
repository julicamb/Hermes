import Vue from 'vue'
import Router from 'vue-router'
import Frontpage from '@/components/Frontpage'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Profile from '@/components/Profile'
import Dashboard from '@/components/Dashboard'
import Messages from '@/components/Messages'
import Conversation from '@/components/Conversation'
import CreateConversation from '@/components/CreateConversation'
import AllFiles from '@/components/AllFiles'
import UserProfile from '@/components/UserProfile'
import ConSettings from '@/components/ConSettings'
import Calendar from '@/components/Calendar'
import Event from '@/components/Event'
import CreateEvent from '@/components/CreateEvent'
import Friends from '@/components/Friends'
import SearchUsers from '@/components/SearchUsers'
import Notifications from '@/components/Notifications'
import MyFiles from '@/components/MyFiles'
import Timeline from '@/components/Timeline'
import CreatePost from '@/components/CreatePost'
import Post from '@/components/Post'
import MyPosts from '@/components/MyPosts'
import MyProjects from '@/components/MyProjects'
import Project from '@/components/Project'
import CreateProject from '@/components/CreateProject'

import firebase from 'firebase';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Frontpage',
      component: Frontpage
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/Dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/Messages',
      name: 'Messages',
      component: Messages
    },
    {
      path: '/Conversation/:id',
      name: 'Conversation',
      component: Conversation
    },
    {
      path: '/CreateConversation/',
      name: 'CreateConversation',
      component: CreateConversation
    },
    {
      path: '/AllFiles/:id/:filter',
      name: 'AllFiles',
      component: AllFiles
    },
    {
      path: '/UserProfile/:id/:type/:source',
      name: 'UserProfile',
      component: UserProfile
    },
    {
      path: '/ConSettings/:id',
      name: 'ConSettings',
      component: ConSettings
    },
    {
      path: '/Calendar',
      name: 'Calendar',
      component: Calendar
    },
    {
      path: '/Event/:id',
      name: 'Event',
      component: Event
    },
    {
      path: '/CreateEvent',
      name: 'CreateEvent',
      component: CreateEvent
    },
    {
      path: '/Friends',
      name: 'Friends',
      component: Friends
    },
    {
      path: '/SearchUsers',
      name: 'SearchUsers',
      component: SearchUsers
    },
    {
      path: '/Notifications',
      name: 'Notifications',
      component: Notifications
    },
    {
      path: '/MyFiles',
      name: 'MyFiles',
      component: MyFiles
    },
    {
      path: '/Timeline',
      name: 'Timeline',
      component: Timeline
    },
    {
      path: '/Post/:id',
      name: 'Post',
      component: Post
    },
    {
      path: '/CreatePost',
      name: 'CreatePost',
      component: CreatePost
    },
    {
      path: '/MyPosts',
      name: 'MyPosts',
      component: MyPosts
    },
    {
      path: '/MyProjects',
      name: 'MyProjects',
      component: MyProjects
    },
    {
      path: '/Project/:id',
      name: 'Project',
      component: Project
    },
    {
      path: '/CreateProject',
      name: 'CreateProject',
      component: CreateProject
    },
  ]
})
